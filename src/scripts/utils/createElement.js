/**
 * Функция должна добавлять html-элементу root потомка childOrChildren
 * @param {HTMLElement} root -- родительский элемент
 * @param {string[] | string | HTMLElement | HTMLElement[]} childOrChildren
 */
function parseChild(root, childOrChildren) {
  /**
   Ваш код
  */
}

/**
 *
 * Функция для валидации переданных параметров для createElement
 * @param {object} params -- параметры createElement
 */
function validateParams(params) {
  /**
   Ваш код
  */
}

/**
 *
 * @param {object} params -- Параметры для создания HTMLElement:
 *   tag -- html-тэг элемента;
 *   attributes -- html-атрибуты элемента;
 *   child -- потомки/потомок
 * @returns HTMLElement
 */
export function createElement(params) {
  //Нужно вынести валидацию параметров в отдельную функцию
  validateParams(params);

  /**
   Ваш код
  */

  //Если в параметрах передан потомок, то мы должны его "распарсить"
  if (child) {
    parseChild(newElement, child);
  }

  return newElement;
}
